output "internal_nginx" {
  value = "${var.chart_name}-nginx.${var.namespace}.svc.cluster.local"
}

output "internal_nginx_url" {
  value = "http://${var.chart_name}-nginx.${var.namespace}.svc.cluster.local"
}

output "internal_push_url" {
  value = "http://${var.chart_name}-nginx.${var.namespace}.svc.cluster.local/api/v1/push"
}

output "internal_prometheus_url" {
  value = "http://${var.chart_name}-nginx.${var.namespace}.svc.cluster.local/prometheus"
}
